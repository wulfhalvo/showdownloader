import re
import sys
import requests, json, urllib
import pathlib
import pickle

def main():
    shows = [
        "hero-elementary",
    ]

    pbsdl = PBSKidsDL(shows=shows)
    pbsdl.run_download()

class PBSKidsDL:
    url_path = "https://content.services.pbskids.org/v2/kidspbsorg/programs/"
    download_path = str(pathlib.Path.home()) + "/Downloads/TV_Shows/"

    already_downloaded_file = ""
    already_downloaded = {}

    def __init__(self, download_path="", shows=[]):
        if len(download_path) > 0:
            self.download_path = download_path
        if len(shows) > 0:
            self.shows = shows

        pathlib.Path(self.download_path).mkdir(parents=True, exist_ok=True)
        self.already_downloaded_file = f"{self.download_path}.pbskidsdownlist"
        self.get_downloaded_episodes()

    def run_download(self):
        for show in self.shows:
            print(show)

            downpath = f"{self.download_path}{show}"
            pathlib.Path(downpath).mkdir(parents=True, exist_ok=True)

            url = f"{self.url_path}{show}"
            data = requests.get(url).json()

            self.download_episodes(data, show, downpath)

            self.put_downloaded_episodes()

    def download_episodes(self, json_str, show, downpath):
        try:
            episodes = json_str["collections"]["episodes"]["content"]
        except:
            return

        for episode in episodes:
            title = episode["title"]
            mp4 = episode["mp4"]

            if show not in self.already_downloaded:
                self.already_downloaded[show] = []
            else:
                if title in self.already_downloaded[show]:
                    continue

            filename = title
            filename = filename.encode("ascii", "ignore").decode()
            filename = str.replace(filename, '/', ', ')
            filename = re.sub(r"\s+", "_", filename)
            filename = "".join(x if x not in "\/:*?<>|" else "_" for x in filename)
            print(f"Downloading {filename}")
            urllib.request.urlretrieve(mp4, '{}/{}.mp4'.format(downpath, filename))

            self.already_downloaded[show].append(title)

    def get_downloaded_episodes(self):
        try:
            with open(self.already_downloaded_file, 'rb') as filehandle:
                self.already_downloaded = pickle.load(filehandle)
        except:
            pass

    def put_downloaded_episodes(self):
        with open(self.already_downloaded_file, 'wb') as filehandle:
            pickle.dump(self.already_downloaded, filehandle)

    def rm_downloaded_episode(self, show, episode):
        if show in self.already_downloaded.keys():
            if episode in self.already_downloaded[show]:
                self.already_downloaded[show].remove(episode)
        self.put_downloaded_episodes()

if __name__ == "__main__":
    main()
